#!/bin/bash
#
# Read the DCL Supervisor log files for the Endurance Surface Moorings and
# create parsed datasets available in JSON formatted files for further
# processing and review.
#
# Wingard, C. 2015-04-17

# Parse the command line inputs
if [ $# -ne 5 ]; then
    echo "$0: required inputs are the platform and deployment names, the name of the DCL, the"
    echo "subassembly [buoy/nsif/mfn] location of the DCL and the name of the file to process."
    echo "     example: $0 ce02shsm D00001 dcl11 buoy 20150505.superv.log"
    exit 1
fi
PLATFORM=${1,,}
DEPLOY=${2^^}
DCL=${3,,}
SUBASY=${4,,}
FILE=`basename $5`

# Set the default directory paths
RAW="/home/ooiuser/data/raw"
PARSED="/home/ooiuser/data/parsed"

# Setup the input and output filenames as well as the absolute paths
IN="$RAW/$PLATFORM/$DEPLOY/cg_data/$DCL/superv/$FILE"
OUT="$PARSED/$PLATFORM/$DEPLOY/$SUBASY/superv/$DCL/${FILE%.log}.json"
if [ ! -d `dirname $OUT` ]; then
    mkdir -p `dirname $OUT`
fi

# Parse the file
if [ -e $IN ]; then
    cd /home/ooiuser/code/cgsn-parsers
    python -m cgsn_parsers.parsers.parse_superv_dcl -i $IN -o $OUT
fi
