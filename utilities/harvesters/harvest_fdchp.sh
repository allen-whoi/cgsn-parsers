#!/bin/bash
#
# Read the raw FDCHP data files for the Endurance Coastal Surface Moorings and
# create parsed datasets available in JSON formatted files for further
# processing and review.
#
# Wingard, C. 2015-04-17

# Parse the command line inputs
if [ $# -ne 3 ]; then
    echo "$0: required inputs are the platform and deployment names,"
    echo "in that order, and the name of the file to process."
    echo "     example: $0 ce02shsm D00001 20150505.fdchp.log"
    exit 1
fi
PLATFORM=${1,,}
DEPLOY=${2^^}
FILE=`basename $3`

# Set the default directory paths
RAW="/home/ooiuser/data/raw"
PARSED="/home/ooiuser/data/parsed"

# Setup the input and output filenames as well as the absolute paths
IN="$RAW/$PLATFORM/$DEPLOY/cg_data/dcl12/fdchp/$FILE"
OUT="$PARSED/$PLATFORM/$DEPLOY/buoy/fdchp/${FILE%.log}.json"

# Parse the file
if [ -e $IN ]; then
    if [ ! -d `dirname $OUT` ]; then
        mkdir -p `dirname $OUT`
    fi
    cd /home/ooiuser/code/cgsn-parsers
    python -m cgsn_parsers.parsers.parse_fdchp -i $IN -o $OUT
fi
